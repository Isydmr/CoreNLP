package edu.stanford.nlp.international.turkish.process;

import java.io.Reader;
import java.util.Locale;
import java.util.Properties;

import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.Label;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.process.CoreLabelTokenFactory;
import edu.stanford.nlp.process.LexedTokenFactory;
import edu.stanford.nlp.process.LexerUtils;
import edu.stanford.nlp.util.logging.Redwood;

/**
 *  A tokenizer for Turkish. 
 *
 *  @author MC &
 */

%%

%class TurkishLexer
%unicode
%function next
%type Object
%char
%caseless

%{

  /**
   * Constructs a new FrenchLexer.  You specify the type of result tokens with a
   * LexedTokenFactory, and can specify the treatment of tokens by boolean
   * options given in a comma separated String
   * (e.g., "invertible,normalizeParentheses=true").
   * If the String is {@code null} or empty, you get the traditional
   * PTB3 normalization behaviour (i.e., you get ptb3Escaping=false).  If you
   * want no normalization, then you should pass in the String
   * "ptb3Escaping=false".  The known option names are:
   * <ol>
   * <li>invertible: Store enough information about the original form of the
   *     token and the whitespace around it that a list of tokens can be
   *     faithfully converted back to the original String.  Valid only if the
   *     LexedTokenFactory is an instance of CoreLabelTokenFactory.  The
   *     keys used in it are TextAnnotation for the tokenized form,
   *     OriginalTextAnnotation for the original string, BeforeAnnotation and
   *     AfterAnnotation for the whitespace before and after a token, and
   *     perhaps BeginPositionAnnotation and EndPositionAnnotation to record
   *     token begin/after end offsets, if they were specified to be recorded
   *     in TokenFactory construction.  (Like the String class, begin and end
   *     are done so end - begin gives the token length.)
   * <li>tokenizeNLs: Whether end-of-lines should become tokens (or just
   *     be treated as part of whitespace)
   * <li>ptb3Escaping: Enable all traditional PTB3 token transforms
   *     (like -LRB-, -RRB-).  This is a macro flag that sets or clears all the
   *     options below.
   * <li>normalizeAmpersandEntity: Whether to map the XML &amp;amp; to an
   *      ampersand
   * <li>normalizeFractions: Whether to map certain common composed
   *     fraction characters to spelled out letter forms like "1/2"
   * <li>normalizeParentheses: Whether to map round parentheses to -LRB-,
   *     -RRB-, as in the Penn Treebank
   * <li>normalizeOtherBrackets: Whether to map other common bracket characters
   *     to -LCB-, -LRB-, -RCB-, -RRB-, roughly as in the Penn Treebank
   * <li>ellipses: [From CoreNLP 4.0] Select a style for mapping ellipses (3 dots).  An enum with possible values
   *     (case insensitive): unicode, ptb3, not_cp1252, original. "ptb3" maps ellipses to three dots (...), the
   *     old PTB3 WSJ coding of an ellipsis. "unicode" maps three dot and optional space sequences to
   *     U+2026, the Unicode ellipsis character. "not_cp1252" only remaps invalid cp1252 ellipses to unicode.
   *     "original" uses all ellipses as they were. The default is ptb3. </li>
   * <li>ptb3Dashes: Whether to turn various dash characters into "--",
   *     the dominant encoding of dashes in the PTB3 WSJ
   * <li>escapeForwardSlashAsterisk: Whether to put a backslash escape in front
   *     of / and * as the old PTB3 WSJ does for some reason (something to do
   *     with Lisp readers??).
   * <li>untokenizable: What to do with untokenizable characters (ones not
   *     known to the tokenizers).  Six options combining whether to log a
   *     warning for none, the first, or all, and whether to delete them or
   *     to include them as single character tokens in the output: noneDelete,
   *     firstDelete, allDelete, noneKeep, firstKeep, allKeep.
   *     The default is "firstDelete".
   * <li>strictTreebank3: PTBTokenizer deliberately deviates from strict PTB3
   *      WSJ tokenization in two cases.  Setting this improves compatibility
   *      for those cases.  They are: (i) When an acronym is followed by a
   *      sentence end, such as "Corp." at the end of a sentence, the PTB3
   *      has tokens of "Corp" and ".", while by default PTBTokenzer duplicates
   *      the period returning tokens of "Corp." and ".", and (ii) PTBTokenizer
   *      will return numbers with a whole number and a fractional part like
   *      "5 7/8" as a single token (with a non-breaking space in the middle),
   *      while the PTB3 separates them into two tokens "5" and "7/8".
   *      (Exception: for "U.S." the treebank does have the two tokens
   *      "U.S." and "." like our default; strictTreebank3 now does that too.)
   * </ol>
   *
   * @param r The Reader to tokenize text from
   * @param tf The LexedTokenFactory that will be invoked to convert
   *    each substring extracted by the lexer into some kind of Object
   *    (such as a Word or CoreLabel).
   * @param props Options to the tokenizer (see constructor Javadoc)
   */
  public TurkishLexer(Reader r, LexedTokenFactory<?> tf, Properties props) {
    this(r);
    this.tokenFactory = tf;
    for (String key : props.stringPropertyNames()) {
      String value = props.getProperty(key);
      boolean val = Boolean.valueOf(value);
      if ("".equals(key)) {
        // allow an empty item
      } else if ("noSGML".equals(key)) {
        noSGML = val;
      } else if ("invertible".equals(key)) {
        invertible = val;
      } else if ("tokenizeNLs".equals(key)) {
        tokenizeNLs = val;
      } else if ("ptb3Escaping".equals(key)) {
        normalizeAmpersandEntity = val;
        normalizeFractions = val;
        normalizeParentheses = val;
        normalizeOtherBrackets = val;
        ellipsisStyle = val ? LexerUtils.EllipsesEnum.PTB3 : LexerUtils.EllipsesEnum.ORIGINAL;
        ptb3Dashes = val;
        quoteStyle = val ? LexerUtils.QuotesEnum.ASCII : LexerUtils.QuotesEnum.ORIGINAL;
      } else if ("quotes".equals(key)) {
        quoteStyle = LexerUtils.QuotesEnum.valueOf(value.toUpperCase());
      } else if ("normalizeAmpersandEntity".equals(key)) {
        normalizeAmpersandEntity = val;
      } else if ("normalizeFractions".equals(key)) {
        normalizeFractions = val;
      } else if ("normalizeParentheses".equals(key)) {
        normalizeParentheses = val;
      } else if ("normalizeOtherBrackets".equals(key)) {
        normalizeOtherBrackets = val;
      } else if ("ellipses".equals(key)) {
        try {
          ellipsisStyle = LexerUtils.EllipsesEnum.valueOf(value.trim().toUpperCase(Locale.ROOT));
        } catch (IllegalArgumentException iae) {
          throw new IllegalArgumentException ("Not a valid ellipses style: " + value);
        }
      } else if ("ptb3Dashes".equals(key)) {
        ptb3Dashes = val;
      } else if ("escapeForwardSlashAsterisk".equals(key)) {
        escapeForwardSlashAsterisk = val;
      } else if ("untokenizable".equals(key)) {
        switch (value) {
          case "noneDelete":
            untokenizable = UntokenizableOptions.NONE_DELETE;
            break;
          case "firstDelete":
            untokenizable = UntokenizableOptions.FIRST_DELETE;
            break;
          case "allDelete":
            untokenizable = UntokenizableOptions.ALL_DELETE;
            break;
          case "noneKeep":
            untokenizable = UntokenizableOptions.NONE_KEEP;
            break;
          case "firstKeep":
            untokenizable = UntokenizableOptions.FIRST_KEEP;
            break;
          case "allKeep":
            untokenizable = UntokenizableOptions.ALL_KEEP;
            break;
          default:
            throw new IllegalArgumentException("FrenchLexer: Invalid option value in constructor: " + key + ": " + value);
        }
      } else if ("strictTreebank3".equals(key)) {
        strictTreebank3 = val;
      } else {
        System.err.printf("%s: Invalid options key in constructor: %s%n", this.getClass().getName(), key);
      }
    }
    // this.seenUntokenizableCharacter = false; // unnecessary, it's default initialized
    if (invertible) {
      if ( ! (tf instanceof CoreLabelTokenFactory)) {
        throw new IllegalArgumentException("FrenchLexer: the invertible option requires a CoreLabelTokenFactory");
      }
      prevWord = (CoreLabel) tf.makeToken("", 0, 0);
      prevWordAfter = new StringBuilder();
    }
  }


  /** Turn on to find out how things were tokenized. */
  private static final boolean DEBUG = false;

  /** A logger for this class */
  private static final Redwood.RedwoodChannels logger = Redwood.channels(FrenchLexer.class);


  private LexedTokenFactory<?> tokenFactory;
  private CoreLabel prevWord;
  private StringBuilder prevWordAfter;
  private boolean seenUntokenizableCharacter;
  private enum UntokenizableOptions { NONE_DELETE, FIRST_DELETE, ALL_DELETE, NONE_KEEP, FIRST_KEEP, ALL_KEEP }
  private UntokenizableOptions untokenizable = UntokenizableOptions.FIRST_DELETE;

  /* Flags begin with historical ptb3Escaping behavior, except do ASCII quotes */
  private boolean invertible;
  private boolean tokenizeNLs;
  private boolean noSGML;
  private boolean normalizeAmpersandEntity = true;
  private boolean normalizeFractions = true;
  private boolean normalizeParentheses;
  private boolean normalizeOtherBrackets;
  private LexerUtils.EllipsesEnum ellipsisStyle = LexerUtils.EllipsesEnum.PTB3;
  private LexerUtils.QuotesEnum quoteStyle = LexerUtils.QuotesEnum.ASCII;
  private boolean ptb3Dashes;
  private boolean escapeForwardSlashAsterisk = false;
  private boolean strictTreebank3;


  /*
   * This has now been extended to cover the main Windows CP1252 characters,
   * at either their correct Unicode codepoints, or in their invalid
   * positions as 8 bit chars inside the iso-8859 control region.
   *
   * ellipsis   85      0133    2026    8230
   * single quote curly starting        91      0145    2018    8216
   * single quote curly ending  92      0146    2019    8217
   * double quote curly starting        93      0147    201C    8220
   * double quote curly ending  94      0148    201D    8221
   * en dash    96      0150    2013    8211
   * em dash    97      0151    2014    8212
   */

  public static final String openparen = "-LRB-";
  public static final String closeparen = "-RRB-";
  public static final String openbrace = "-LCB-";
  public static final String closebrace = "-RCB-";
  public static final String ptbmdash = "--";
  public static final String NEWLINE_TOKEN = "*NL*";
  public static final String COMPOUND_ANNOTATION = "comp";
  public static final String CONTR_ANNOTATION = "contraction";


  private static String asciiDash(String in) {
    return in.replaceAll("[_\u058A\u2010\u2011]","-");
  }

  private Object getNext() {
    final String txt = yytext();
    return getNext(txt, txt);
  }

  /** Make the next token.
   *  @param txt What the token should be
   *  @param originalText The original String that got transformed into txt
   */
  private Object getNext(String txt, String originalText) {
    return getNext(txt, originalText, null);
  }

  private Object getNext(String txt, String originalText, String annotation) {
    txt = LexerUtils.removeSoftHyphens(txt);
    Label w = (Label) tokenFactory.makeToken(txt, yychar, yylength());
    if (invertible || annotation != null) {
      CoreLabel word = (CoreLabel) w;
      if (invertible) {
        String str = prevWordAfter.toString();
        prevWordAfter.setLength(0);
        word.set(CoreAnnotations.OriginalTextAnnotation.class, originalText);
        word.set(CoreAnnotations.BeforeAnnotation.class, str);
        prevWord.set(CoreAnnotations.AfterAnnotation.class, str);
        prevWord = word;
      }
      if (annotation != null) {
        word.set(CoreAnnotations.ParentAnnotation.class, annotation);
      }
    }
    return w;
  }

  private Object getNormalizedAmpNext() {
    final String txt = yytext();
    return normalizeAmpersandEntity ?
      getNext(LexerUtils.normalizeAmp(txt), txt) : getNext();
  }

%}


%{
  String name;
%}

space = [\ \t\r\n]
endPunct = [\.\!\?]
otherPunct = [\,\;\:]

%%

{space}	{return getNext();}
{endPunct}	{return getNext(); }
{otherPunct}	{return getNext(); }
.	{return getNext();}